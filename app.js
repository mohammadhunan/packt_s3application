const express = require('express'),
  app = express(),
  AWS = require('aws-sdk'),
  fs =  require('fs'),
  key = require('./config');
  var multer  = require('multer')
  var upload = multer()



  var s3 = new AWS.S3();
  var globalBucket = 'pstorageapp'



  AWS.config.update(
    {
      accessKeyId: key.AWS.accessKeyId,
      secretAccessKey: key.AWS.secretAccessKey,
      region: key.AWS.region
    }
  );




  var storage = multer.diskStorage({
  	destination: function(req, file, callback) {
  		callback(null, './tempstorage')
  	},
  	filename: function(req, file, callback) {
  		callback(null, file.originalname)
  	}
  })



  function uploadToS3(file,req,res){
    fs.readFile(__dirname + '/tempstorage/' + file,(err,data) => {
      if(err) throw err;
      var params = { Bucket: globalBucket, Key: file, Body: data,   ACL: 'public-read'}
      var putObjectPromise = s3.putObject(params).promise();
      putObjectPromise.then(function(data) {
        console.log('Successfully uploaded');


          var params = { Bucket: globalBucket, Key: file }
          var getObjectPromise = s3.getObject(params).promise();
          getObjectPromise.then(function(data) {
            console.log('read from s3');

            let j = (data.Body).toString('utf8')
            res.send(j);

          }).catch(function(err) {
            console.log(err);
          });






      }).catch(function(err) {
        console.log(err);
      });
  })
}

app.get("/", (req,res) => {
  res.sendFile(__dirname + "/index.html");
})


app.post('/api/file', function(req, res) {
	var upload = multer({
		storage: storage
	}).single('userFile')
	upload(req, res, function(err) {
    uploadToS3(req.file.filename,req,res);
	})
})

app.listen(3000,()=>{
  console.log('listening on port: 3000');
})
