var params = { Bucket: globalBucket, Key: file, Body: data,   ACL: 'public-read'}
var putObjectPromise = s3.putObject(params).promise();
putObjectPromise.then(function(data) {
  console.log('Successfully uploaded');
}).catch(function(err) {
  console.log(err);
});
